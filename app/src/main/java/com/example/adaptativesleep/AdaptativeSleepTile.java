package com.example.adaptativesleep;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.Settings;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

public class AdaptativeSleepTile extends TileService {

    @Override
    public void onClick() {
        super.onClick();

        String s = getApplicationContext().getPackageName();

        boolean authorized = Settings.System.canWrite(this);
        if (!authorized) {
            openAndroidPermissionMenu();
        }
        else {
//            Toast.makeText(this, "Yes permission", Toast.LENGTH_LONG).show();

            Tile tile = getQsTile();
            int state = tile.getState();

            if (state == Tile.STATE_UNAVAILABLE || state == Tile.STATE_INACTIVE) {
                tile.setState(Tile.STATE_ACTIVE);

                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 5*60*1000);

            } else {
                tile.setState(Tile.STATE_INACTIVE);

                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 30000);
            }
            tile.updateTile();
        }
    }

    @Override
    public void onTileAdded() {
        super.onTileAdded();
        Toast.makeText(this, "Tile added", Toast.LENGTH_LONG).show();
        getQsTile().setState(Tile.STATE_INACTIVE);
    }

    @Override
    public void onTileRemoved() {
        super.onTileRemoved();
        Toast.makeText(this, "Tile removed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStartListening() {
        super.onStartListening();
        //Toast.makeText(this, "Tile listens", Toast.LENGTH_LONG).show();
        //When tile becomes visible
    }

    @Override
    public void onStopListening() {
        super.onStopListening();
        //when tile is no longer visible
    }

    private void openAndroidPermissionMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivityAndCollapse(intent);

//        String message = "Veuillez autoriser l'application à modifier les paramètres systèmes";
//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

    }

}
