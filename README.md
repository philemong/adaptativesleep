# Adaptative Sleep
Mini application Android permettant de modifier le temps avant mise en veille de l'écran via une tuile du menu d'accès rapide.

Le temps de veille est hardcodé : désactivé −> 30 seconde / activé −> 5 minutes.

Parceque j'aime me compliquer la vie, ce projet (en version 1.0) ne contient aucune activité : il y a seulement la tuile. Cela impose quelques limites, mais j'aime l'extrème simplicité du résultat.